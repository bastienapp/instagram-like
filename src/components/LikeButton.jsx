import { useState } from "react";

function LikeButton(props) {
  // <></> <></> <></> <></>
  const [like, setLike] = useState(false);

  const toggleLike = () => {
    // point d'exclamation (!) : inverser la valeur d'un booléen
    setLike(!like);
  };

  return (
    <>
      <button onClick={toggleLike}>
        {/* like / dislike */}
        { like === true ? '🤍' : '🤎' }
      </button>
    </>
  );
}

export default LikeButton;
