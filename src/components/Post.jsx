import LikeButton from "./LikeButton";

function Post(props) {
  /*
  props.title
  props.date
  props.image
  */
  return (
    <>
      <h2>{props.title}</h2>
      <img
        src={props.image}
        alt='antilopes'
      />
      <LikeButton></LikeButton>
      <p>{props.date}</p>
    </>
  );
}

export default Post;
