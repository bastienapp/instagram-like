import Post from "./components/Post";
import instagramLogo from "./assets/Instagram_icon.png";

function App() {
  // image : Antilopes https://images.unsplash.com/photo-1682687220305-ce8a9ab237b1

  // airbnb : https://images.unsplash.com/photo-1702047095842-01ca5148d1b0

  return (
    <>
      <h1>Instagram</h1>
      <img src={instagramLogo} alt="Logo d'instagram" />
      <Post
        image='https://images.unsplash.com/photo-1682687220305-ce8a9ab237b1'
        title='Antilopes'
        date='13 décembre 2023'
      />
      <Post
        image='https://images.unsplash.com/photo-1702047095842-01ca5148d1b0'
        title='airbnb'
        date='8 octobre 2022'
      />
    </>
  );
}

export default App;
