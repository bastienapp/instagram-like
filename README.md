Faire un site de réseau social à la Instagram, qui afficherait :

- dans app, créer un post, qui comporterait un titre, une date, une photo (de https://unsplash.com) et un message

- ensuite, créer déplacer le code dans un composant qui recevrait les informations du post à partir des props

- ensuite, il doit être possible de liker le post

- ensuite, il doit être possible d'afficher une liste de post et non un seul

- ensuite, chaque post doit pouvoir afficher une liste de commentaire, qui ont chacun le nom de l'utilisateur, et le message. Les commentaires auront leur propre composant
